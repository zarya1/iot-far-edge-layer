/*****************************
testing mpu-6050 on ATmega328
*****************************/

#include <Wire.h>

const int MPU_ADDR = 0x68;

float accel_x; 
float accel_y; 
float accel_z; 
float gyro_x; 
float gyro_y; 
float gyro_z; 
float temp; 

void setup(){
  Serial.begin(9600);  
  Wire.begin();
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x6B); 
  Wire.write(0); 
  Wire.endTransmission(true); 
}

void loop(){
  Wire.beginTransmission(MPU_ADDR);
  Wire.write(0x3B);
  Wire.endTransmission(false);

  Wire.requestFrom(MPU_ADDR, 14, true);

  accel_x = Wire.read() << 8 | Wire.read();
  accel_y = Wire.read() << 8  | Wire.read();
  accel_z = Wire.read() << 8 | Wire.read();

  temp = Wire.read() << 8 | Wire.read();

  gyro_x = Wire.read() << 8 | Wire.read();
  gyro_y = Wire.read() << 8  | Wire.read();
  gyro_z = Wire.read() << 8 | Wire.read();

  accel_x = accel_x / 16384.0;
  accel_y = accel_y / 16384.0;
  accel_z = accel_z / 16384.0;

  temp = temp / 340.00 + 36.53; 

  gyro_x = gyro_x / 131.0;
  gyro_y = gyro_y / 131.0;
  gyro_z = gyro_z / 131.0;

  Serial.print("AcX: ");
  Serial.print(accel_x, 2);
  Serial.print(" | AcY: ");
  Serial.print(accel_y, 2);
  Serial.print(" | AcZ: ");
  Serial.print(accel_z, 2);
  Serial.print("");

  Serial.print(" | Tmp: ");
  Serial.print(temp);

  Serial.print(" | GyX: ");
  Serial.print(gyro_x, 2);
  Serial.print(" | GyY: ");
  Serial.print(gyro_y, 2);
  Serial.print(" | GyZ: ");
  Serial.print(gyro_z, 2);

  Serial.println();

  delay(500);
}
